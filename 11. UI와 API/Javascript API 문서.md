# Javascript API 문서
- [생활코딩 자바스크립트 사전](https://opentutorials.org/course/50)
- [모질라 자바스크립트 레퍼런스](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference)

# 호스트 환경 API 문서
- [웹브라우저 API](https://developer.mozilla.org/en-US/docs/Web/API)
- [Node.js API](https://nodejs.org/api/)