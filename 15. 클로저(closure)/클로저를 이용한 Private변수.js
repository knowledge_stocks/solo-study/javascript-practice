function factory_movie(title){
  return {
      get_title : function (){
          return title;
      },
      set_title : function(_title){
          title = _title
      }
  }
}

// 아래 ghost와 matrix라는 두 객체에는 title이라는 맴버변수는 없지만,
// 이 객체 내부에서는 title을 사용하고 있음.
// 즉 Private 변수처럼 사용
ghost = factory_movie('Ghost in the shell');
matrix = factory_movie('Matrix');

ghost.title = '공각기동대'; // ghost 내부에서 사용하고 있는 title이 아닌, 새 맴버변수로 title이 생성됨

console.log(ghost.get_title());
console.log(matrix.get_title());

ghost.set_title('공각기동대');

console.log(ghost.get_title());
console.log(matrix.get_title());