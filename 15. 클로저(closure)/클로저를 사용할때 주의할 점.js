var arr = []
for(var i = 0; i < 5; i++){
  arr[i] = function(){
    return i; // 이놈이 참조하고 있는 i는 밖에서 5까지 상승함
  }
}
for(var index in arr) {
  console.log(arr[index]()); // 5만 출력됨
}
/* 결과
5
5
5
5
5
*/
console.log();

// c#이나 java같은 다른 언어에서 하는 방식으로 해결하려고 하면 안 됨
var arr = []
for(var i = 0; i < 5; i++){
  var j = i; // javascript는 블록에 의한 지역변수는 지원하지 않음. 따라서 for문 안에서 j를 생성해도 이 j는 전역변수로 생성됨
  arr[i] = function(){
    return j; // 이놈이 참조하고 있는 j는 밖에서 최종적으로 4까지 상승함
  }
}
for(var index in arr) {
  console.log(arr[index]());
}
/* 결과
4
4
4
4
4
*/
console.log();

var arr = []
for(var i = 0; i < 5; i++){
  arr[i] = function(id) {
    return function(){
      return id; // 2. 입력된 id값을 출력함
    }
  }(i); // 1. 반복문이 돌아가는 당시의 i값이 id로 입력
}
for(var index in arr) {
  console.log(arr[index]());
}
/* 결과
0
1
2
3
4
*/