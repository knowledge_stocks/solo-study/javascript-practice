// 내부함수는 외부함수의 지역변수에 접근할 수 있는데
// 외부 함수의 실행이 끝나서 외부함수가 소멸된 이후에도 내부함수가 외부함수의 변수에 접근할 수 있다.
// 이러한 메커니즘을 클로저라고 한다.

function outter() {
  var title = 'coding everybody';
  return function() {
    console.log(title);
  }
}
var inner = outter(); // 외부 함수는 여기에서 호출되고 내부함수를 반환하고 종료됨
inner(); // 외부함수가 종료된 상태에서 title 변수를 참조하고 있는 내부함수를 실행