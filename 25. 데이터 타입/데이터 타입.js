// # 원시 데이터 타입(Primitive type, 기본 데이터 타입)
// - 숫자
// - 문자열
// - 불리언
// - null
// - undefined

// # 객체 데이터 타입(객체 데이터 타입)
// - Object
// - Array
// - Number
// - String
// - Boolean
// - 등등

// # 레퍼 객체(Wrapper Object)
var str = 'coding'; // new Object(coding)와 같지 않다.
console.log(str.length);
console.log(str.charAt(0));

str.prop1 = 'everybody'; // 언뜻 되는것처럼 보이지만 (이 순간에만 잠깐 str이라는 원시 데이터를 객체로 만들었다가, 줄이 끝나면 제거한다.)
console.log(str.prop1 + ''); // 접근해보면 undefined가 출력된다.

// 위의 코드를 보면 문자열은 프로퍼티와 메소드가 있는 객체처럼 보인다.
// 이는 String이라는 객체가 원시 데이터인 문자열을 내부적으로 처리하고 있기 때문이다.
// 이러한 객체들을 '레퍼 객체'라고 한다.