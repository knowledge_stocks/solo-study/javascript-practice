function a() {}; // a = function() {} // a라는 변수에 함수를 넣음
console.log(a);

// 함수는 객체처럼 사용할 수 있다.
a = function() { console.log('a'); };
a.b = function() { console.log('b') }; // a는 함수이지만, 동시에 객체. 다른 속성들을 가질 수 있다
console.log(a);
a(); // a
a.b(); // b

// 함수를 매개변수로 전달할 수 있다.
function cal(func, num)
{
  return func(num);
}
function increase(num)
{
  return num+1;
}
function decrease(num)
{
  return num-1;
}
console.log(cal(increase, 10)); // 11
console.log(cal(decrease, 10)); // 9

var funcs = {
  plus : function(left, right){return left + right},
  minus : function(left, right){return left - right}
}
function cal2(mode){
  return funcs[mode];
}
console.log(cal2('plus')(2,1));
console.log(cal2('minus')(2,1));