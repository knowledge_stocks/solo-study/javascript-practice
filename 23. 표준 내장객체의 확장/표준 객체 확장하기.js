Array.prototype.rand = function(){ // 표준 객체인 Array의 prototype에 rand 메소드를 추가한다.
  var index = Math.floor(this.length*Math.random());
  return this[index];
}

var arr = new Array('seoul','new york','ladarkh','pusan', 'Tsukuba');
var arr2 = ['seoul','new york','ladarkh','pusan', 'Tsukuba'];

console.log(arr.rand());
console.log(arr2.rand());