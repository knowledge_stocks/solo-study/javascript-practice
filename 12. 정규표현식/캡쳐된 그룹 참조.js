var pattern = /(\w+)\s(\w+)/g;
var str = 'coding everybody coding';

console.log(str.replace(pattern, '$& / $2 $1')); // $로 캡쳐된 그룹을 참조할 수 있다. $&는 매칭된 전체 부분을 의미한다.

var pattern2 = /(\w+)\s(\w+)\s$1/g; // 정규식 안에 바로 사용은 안 되는 듯 하다. 정규식에서 $는 문자열의 끝과 매칭된다. 고로 잘 못 된 정규식.
console.log(pattern2.test(str));

var str2 = 'coding everybody $1';
console.log(pattern2.test(str2));

var pattern3 = /(\w+)\s(\w+)\s\$1/g;

console.log(pattern3.test(str2));