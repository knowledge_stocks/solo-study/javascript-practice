function Person1(name){
  this.name = name;
  this.introduce = function(){
      return 'My name is '+this.name;
  }
}
var p1 = new Person1('egoing');
document.write(p1.introduce()+"<br />");


function Person(name){
  this.name = name;
  // 여기 있던 기본 코드를
}
// prototype이라는 속성에 넣음.
// prototype은 뼈대, 클래스를 의미한다고 보면 된다.
// 위에서 prototype 없이 만든 Persion1은 뼈대가 없는 생성자, 클래스이다.
Person.prototype.name=null;
Person.prototype.introduce = function(){
  return 'My name is '+this.name;
}
var p = new Person('egoing');
document.write(p.introduce()+"<br />");